﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadSpawnCollider : MonoBehaviour {
	public GameObject playerCar;
	public GameObject roadSpawnerObj;
	public GameObject busSpawnerObj;

	ObstacleSpawner busSpawner;

	RoadSpawner spawner;

	void Start() {
		spawner = roadSpawnerObj.GetComponent<RoadSpawner>();
		busSpawner = busSpawnerObj.GetComponent<ObstacleSpawner>();
	}

	// Use this for initialization
	public void OnTriggerEnter(Collider other) {
		if(other.transform.gameObject.tag == "PlayerCar") {
			spawner.SpawnNewRoad(this.transform.parent.position);
			this.gameObject.SetActive(false);
			busSpawner.SpawnObstacleAtPosZ(this.transform.parent.position.z + 360);
		}
	}
}
