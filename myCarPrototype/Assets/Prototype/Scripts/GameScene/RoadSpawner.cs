﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadSpawner : MonoBehaviour {

	const int DIST_BETWEEN_ROAD = 160;
	public List<GameObject> RoadObjPool;	
	int currentRoadIndex;

	// Use this for initialization
	void Start () {
		currentRoadIndex = 0;
	}

	public void SpawnNewRoad(Vector3 CollidedRoadPos) {
		GameObject reusedRoad = RoadObjPool[currentRoadIndex];
		Vector3 newRoadPos = new Vector3(0,0,CollidedRoadPos.z + DIST_BETWEEN_ROAD*3);
		reusedRoad.transform.position = newRoadPos;
		reusedRoad.transform.Find("SpawnColider").gameObject.SetActive(true);
		currentRoadIndex ++;
		if(currentRoadIndex == 4) {
			currentRoadIndex = 0;
		}

	}
}
