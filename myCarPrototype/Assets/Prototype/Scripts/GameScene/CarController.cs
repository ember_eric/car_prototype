﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour {
	public float laneWidth;

	[Header("Speed")]
	public float carSpeed;
	public float topCarSpeed;
	public float carSpeedIncreaseRate;

	[Header("Collision")]
	public float controlDisableTime;
	public bool isCollideWithObstacle;

	float myCurrentCarSpeed;
	Vector2 initialTouchPos;
	Vector2 endTouchPos;
	Vector3 moveDirection;

	int laneNo = 2;
	int nextLaneNo = -1;
	List<float> lanePosList;
	bool isChangingLane = false;

	// Use this for initialization
	void Start () {
		myCurrentCarSpeed = carSpeed;
		lanePosList = new List<float>();

		//Lane 3 has x pos as 0
		lanePosList.Add(-2*laneWidth);
		lanePosList.Add(-laneWidth);
		lanePosList.Add(0);
		lanePosList.Add(laneWidth);
		lanePosList.Add(2*laneWidth);

	}
	
	// Update is called once per frame
	void Update () {
		myCurrentCarSpeed += carSpeedIncreaseRate*Time.deltaTime;
		if(myCurrentCarSpeed > topCarSpeed) {
			myCurrentCarSpeed = topCarSpeed;
		}

		//Handling swap
		if(!isChangingLane) {
			moveDirection = new Vector3(lanePosList[laneNo],2,transform.position.z+10);
			if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began) {
				initialTouchPos = Input.GetTouch(0).position;
			}
			else if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended) {
				endTouchPos = Input.GetTouch(0).position;
				if(Mathf.Abs(initialTouchPos.x-endTouchPos.x) > 10) {
					if(initialTouchPos.x < endTouchPos.x) {
						if((laneNo) + 1 < 5) {
							isChangingLane = true;
							nextLaneNo = laneNo+1;
							moveDirection = new Vector3(lanePosList[nextLaneNo],2,transform.position.z+5);
						}
					}
					else if(initialTouchPos.x > endTouchPos.x ){
						if((laneNo - 1) > -1) {
							isChangingLane = true;
							nextLaneNo = laneNo-1;
							moveDirection = new Vector3(lanePosList[nextLaneNo],2,transform.position.z+5);
						}
					}
				}
			}
		}
		else {
			if(transform.position.x == lanePosList[nextLaneNo]) {
				isChangingLane = false;
				laneNo = nextLaneNo;
				nextLaneNo = -1;
			}
		}
		transform.position = Vector3.MoveTowards(transform.position, moveDirection, myCurrentCarSpeed*Time.deltaTime);
	}

	public void CarMeshCollidesWithObstacle() {
		myCurrentCarSpeed = 20;
	}
}
