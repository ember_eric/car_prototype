﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour {
	public GameObject BusPrefab;
	List<GameObject> busPool;

	void Awake() {
		busPool = new List<GameObject>();
		for(int i = 0 ; i < 12 ; i++) {
			GameObject myBus = Instantiate(BusPrefab, this.transform) as GameObject;
			myBus.SetActive(false);
			busPool.Add(myBus);
		}
	}

	public void SpawnObstacleAtPosZ(float zPos) {
		int noOfBus = Random.Range(2,4);
		List<int> laneSet = new List<int>();
		laneSet.Add(0);
		laneSet.Add(1);
		laneSet.Add(2);
		laneSet.Add(3);
		laneSet.Add(4);

		for(int i = 0 ; i < noOfBus ; i++) {
			GameObject bus = null;
			for(int j = 0 ; j < busPool.Count; j++) {
				if(!busPool[j].activeInHierarchy) {
					bus = busPool[j];
				}
			}

			if(bus) {
				int randIndex = Random.Range(0, laneSet.Count);
				int laneNo = laneSet[randIndex];
				laneSet.RemoveAt(randIndex);
				int randZ = Random.Range(30,90);
				Vector3 newPos = new Vector3((-18 + laneNo*9), 0 ,zPos+randZ);
				bus.transform.position = newPos;
				bus.SetActive(true);
			}
		}			
	}
}
