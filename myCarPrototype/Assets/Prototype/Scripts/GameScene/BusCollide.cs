﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BusCollide : MonoBehaviour {
	GameObject playerCarObj;
	CarController carController;

	private void Start() {
		playerCarObj = GameObject.Find("PlayerCar");
		carController = playerCarObj.GetComponent<CarController>();	
	}

	private void OnTriggerEnter(Collider other) {
		if(other.gameObject.tag == "BusCollector") {
			this.transform.parent.gameObject.SetActive(false);
		}
		else if(other.gameObject.tag == "PlayerCar") {
			carController.CarMeshCollidesWithObstacle();
		}
	}
}
